﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            bool rainy = true; //Attribution d'une valeur Vrai à ma variable.
            if (!rainy) // Je vérifie que la valeur de ma variable est fausse ("!").
            { Console.WriteLine("Bonjour, il ne pleut pas aujourd’hui, bonne journée."); } 
            else
            { Console.WriteLine("Bonjour, il pleut aujourd’hui, vous devriez prendre votre parapluie."); }

        }
    }
}
